import express from 'express'
import cors from 'cors'

const app = express();
app.use(cors());

function canonize(url) {
  const re = new RegExp('(?:https?:)?(?:\/\/)?(?:[a-zA-Z0-9._-]+[^\/]*\/)?(?:\@)?([a-zA-Z0-9._-]*)', 'i');
  const username = url.match(re);
  console.log(username);
  return '@' + username[1];
}

app.get('/Task2c', function (req, res) {

  const username = canonize(req.query.username);
    res.send(username);
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
